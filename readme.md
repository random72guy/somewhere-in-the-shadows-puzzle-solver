Somewhere in the Shadows Puzzle Solver
===

Uses linear programming to solve [this puzzle](https://youtu.be/R1to2CoLeWs?t=899) in the game "Somewhere in the Shadows".

![The prompt for the puzzle.](prompt.png)

To run the script: `npm start`

## V2 - Genetic Algorithm

Linear programming achieves the wrong solution, because the actual problem requires multiplying the variables. A genetic algorithm would be more adaptable to this use case. Some potential libraries include:

* [genetic algorithm - npm search](https://www.npmjs.com/search?q=genetic%20algorithm)
* [propsat - npm](https://www.npmjs.com/package/propsat)
* [@coool/genetics - npm](https://www.npmjs.com/package/@coool/genetics)