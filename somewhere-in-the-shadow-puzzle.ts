import * as k from '@lume/kiwi'
// Algorithm Docs: https://kiwisolver.readthedocs.io/en/latest/basis/basic_systems.html

const t = new k.Variable();
const s = new k.Variable();
const x = new k.Variable();
const tstsxxxx = new k.Expression(t,s,t,s,x,x,x,x);

const solver = new k.Solver();
solver.addEditVariable(t, k.Strength.strong);
solver.addEditVariable(s, k.Strength.strong);
solver.addEditVariable(x, k.Strength.strong);

solver.addConstraint(new k.Constraint(
    new k.Expression(t,t,x),
    k.Operator.Eq,
    10,
))

solver.addConstraint(new k.Constraint(
    new k.Expression(s,t,t,x,x),
    k.Operator.Eq,
    38,
))

solver.addConstraint(new k.Constraint(
    new k.Expression(t,s,s,x,x,x),
    k.Operator.Eq,
    51,
))

solver.updateVariables();

console.log({
    t: t.value(),
    s: s.value(),
    x: x.value(),
    tstsxxxx: tstsxxxx.value(),
})
